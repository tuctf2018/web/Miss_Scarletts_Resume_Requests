# Miss Scarlett's Resume Requests
Something is up with Miss's Scarlet's acting site. Maybe you can take a look?

This challenge was meant to introduce HTTP POST requests. 
After visiting the contact.html page, you realize you need to visit /Boddy, similar to with Mrs. Whites's.
Viewing that index.html page via GET request redirects to nope.html, where in the comments it states
```Maybe look into how easy it would be to receive some tissues in the 'post'```
Issuing a post request to /Boddy yields the flag.